﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using MoreMountains.Feedbacks;
using MoreMountains.Tools;
using MoreMountains.TopDownEngine;

public class GreedRushLevelManager : MultiplayerLevelManager, MMEventListener<PickableItemEvent>
{
    public struct GreedPoints
    {
        public string PlayerID;
        public int Points;
    }

    [Header("GreedRush Bindings")]
    public GreedPoints[] Points;
    public List<MMCountdown> Countdowns;

    [Header("Multiplayer Respawn")]
    public float DelayBeforeRespawn = 5;
    public List<MMCountdown> RespawnCountdowns;
    
    [Header("GreedRush Settings")]
    public int GameDuration = 99;
    public float BeforeCoinSpawned = 3f;
    public List<GameObject> GoldCoins;

    public string WinnerID { get; set; }
    protected string _playerID;
    protected string _deadPlayerID;
    protected bool _gameOver = false;
    protected bool _coinSpawned = false;
    protected int _randomCoin = 0;

    protected override void Initialization()
    {
        base.Initialization();
        WinnerID = "";
        Points = new GreedPoints[Players.Count];
        int i = 0;
        foreach (Character player in Players)
        {
            Points[i].PlayerID = player.PlayerID;
            Points[i].Points = 0;
            i++;
        }
        foreach (MMCountdown countdown in Countdowns)
        {
            countdown.CountdownFrom = GameDuration;
            countdown.ResetCountdown();
        }
        foreach(GameObject coins in GoldCoins)
        {
            coins.gameObject.SetActive(false);
        }
        foreach(MMCountdown respawncountdowns in RespawnCountdowns)
        {
            respawncountdowns.CountdownFrom = DelayBeforeRespawn+1;
            respawncountdowns.CountdownTo = 1;
        }
    }

    protected override void OnPlayerDeath(Character playerCharacter)
    {
        base.OnPlayerDeath(playerCharacter);
        TopDownEngineEvent.Trigger(TopDownEngineEventTypes.RespawnStarted, playerCharacter);
    }

    public void RespawnPlayer(string playerID)
    {
        for(int i = 0; i < Players.Count; i++)
        {
            if(playerID == Players[i].PlayerID)
            {
                SpawnPoints[i].SpawnPlayer(Players[i]);
                TopDownEngineEvent.Trigger(TopDownEngineEventTypes.RespawnComplete, Players[i]);
            }
        }
    }

    protected virtual IEnumerator GameOver()
    {
        yield return new WaitForSeconds(2f);
        if (WinnerID == "")
        {
            WinnerID = "Player1";
        }
        MMTimeScaleEvent.Trigger(MMTimeScaleMethods.For, 0f, 0f, false, 0f, true);
        _gameOver = true;
        TopDownEngineEvent.Trigger(TopDownEngineEventTypes.GameOver, null);
    }

    public override void Update()
    {
        base.Update();
        UpdateCountdown();
        CheckForGameOver();
        CheckCoins();
    }

    protected virtual void UpdateCountdown()
    {
        if (_gameOver)
        {
            return;
        }

        float remainingTime = GameDuration;
        foreach (MMCountdown countdown in Countdowns)
        {
            if (countdown.gameObject.activeInHierarchy)
            {
                remainingTime = countdown.CurrentTime;
            }
        }
        if (remainingTime <= 0f)
        {
            int maxPoints = 0;
            foreach (GreedPoints points in Points)
            {
                if (points.Points > maxPoints)
                {
                    WinnerID = points.PlayerID;
                    maxPoints = points.Points;
                }
            }
            StartCoroutine(GameOver());
        }
    }

    protected virtual void CheckForGameOver()
    {
        if (_gameOver)
        {
            if ((Input.GetButton("Player1_Jump"))
                || (Input.GetButton("Player2_Jump"))
                || (Input.GetButton("Player3_Jump"))
                || (Input.GetButton("Player4_Jump")))
            {
                LoadingSceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }
    }

    protected virtual void CheckCoins()
    {
        _coinSpawned = false;
        
        foreach (GameObject coins in GoldCoins)
        {
            if (coins.gameObject.activeInHierarchy)
            {
                _coinSpawned = true;
                break;
            }
        }

        if (_coinSpawned)
        {
            return;
        }
        else
        {
            StartCoroutine(SpawningCoins());
        }
    }

    protected virtual IEnumerator SpawningCoins()
    {
        _randomCoin = Random.Range(0, GoldCoins.Count);
        yield return new WaitForSeconds(BeforeCoinSpawned);
        GoldCoins[_randomCoin].gameObject.SetActive(true);
        _coinSpawned = true;
    }

    public virtual void OnMMEvent(PickableItemEvent pickEvent)
    {
        _playerID = pickEvent.Picker.GetComponentNoAlloc<Character>()?.PlayerID;
        for (int i = 0; i < Points.Length; i++)
        {
            if (Points[i].PlayerID == _playerID)
            {
                Points[i].Points++;
                TopDownEngineEvent.Trigger(TopDownEngineEventTypes.Repaint, null);
            }
        }
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        this.MMEventStartListening<PickableItemEvent>();
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        this.MMEventStopListening<PickableItemEvent>();
    }

}
