﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using MoreMountains.TopDownEngine;

public class GreedRushSpawnpoints : CheckPoint
{
    public void RespawnPlayer(Character player)
    {
        StartCoroutine(StartRespawning(player));
    }

    protected IEnumerator StartRespawning(Character player)
    {
        yield return new WaitForSeconds(GreedRushLevelManager.Instance.DelayBeforeDeathScreen);
        SpawnPlayer(player);
    }
}
